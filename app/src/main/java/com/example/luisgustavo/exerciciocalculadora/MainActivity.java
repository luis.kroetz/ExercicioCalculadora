package com.example.luisgustavo.exerciciocalculadora;

import android.content.Context;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    /* Componentes que exibirão os números inseridos pelo usuário. */
    TextView campoDeTextoResultado;

    /* Teclado numérico. */
    Button botaoZero;
    Button botaoUm;
    Button botaoDois;
    Button botaoTres;
    Button botaoQuatro;
    Button botaoCinco;
    Button botaoSeis;
    Button botaoSete;
    Button botaoOito;
    Button botaoNove;

    /* Componente para números decimais. */
    Button botaoDecimal;

    /* Componentes que efetuam as quatro operações matemáticas. */
    Button botaoSoma;
    Button botaoSubtrai;
    Button botaoMultiplica;
    Button botaoDivide;

    /* Componentes que permite dar preferência aos cálculos. */
    Button botaoPorcentagem;

    /* Componente para limpar o visor e outro para limpar último digito. */
    Button botaoLimpa;
    Button botaoDesfazer;

    /* Componente que exibe o resultado. */
    Button botaoResultado;

    /* Strings utilizadas. */
    private String equacao = "";
    private String part1 = "";
    private String part2 = "";
    private String operador = "";

    /* Variáveis que recebem os valores. */
    private double valor1 = 0;
    private double valor2 = 0;
    private double resultado = 0;

    /* Variáveis para condicoes. */
    boolean status = false;
    boolean teste = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        campoDeTextoResultado = (TextView) findViewById(R.id.texto_resultado);

        botaoZero = (Button) findViewById(R.id.botao_zero);
        botaoUm = (Button) findViewById(R.id.botao_um);
        botaoDois = (Button) findViewById(R.id.botao_dois);
        botaoTres = (Button) findViewById(R.id.botao_tres);
        botaoQuatro = (Button) findViewById(R.id.botao_quatro);
        botaoCinco = (Button) findViewById(R.id.botao_cinco);
        botaoSeis = (Button) findViewById(R.id.botao_seis);
        botaoSete = (Button) findViewById(R.id.botao_sete);
        botaoOito = (Button) findViewById(R.id.botao_oito);
        botaoNove = (Button) findViewById(R.id.botao_nove);

        botaoDecimal = (Button) findViewById(R.id.botao_ponto);

        botaoSoma = (Button) findViewById(R.id.botao_adicao);
        botaoSubtrai = (Button) findViewById(R.id.botao_subtracao);
        botaoMultiplica = (Button) findViewById(R.id.botao_multiplicacao);
        botaoDivide = (Button) findViewById(R.id.botao_divisao);

        botaoPorcentagem = (Button) findViewById(R.id.botao_porcentagem);

        botaoLimpa = (Button) findViewById(R.id.botao_limpa);
        botaoDesfazer = (Button) findViewById(R.id.botao_desfazer);

        botaoResultado = (Button) findViewById(R.id.botao_igual);

    }

    public void botaoZero(View view) {
        /* Se operador não tiver sido inserido. */
        if (operador.isEmpty()) {
            part1 = part1 + "0"; /* String1 recebe o valor. */
        }
        /* Caso operador tenha sido inserido. */
        else {
            part2 = part2 + "0"; /* String2 recebe o valor. */
        }
        teste = true;
        equacao = equacao + "0"; /* Equação recebe o número e soma com o que já tem na string.*/
        campoDeTextoResultado.setText(equacao);
    }

    public void botaoUm(View view) {
        if (operador.isEmpty()) {
            part1 = part1 + "1";
        } else {
            part2 = part2 + "1";
        }
        teste = true;
        equacao = equacao + "1";
        campoDeTextoResultado.setText(equacao);
    }

    public void botaoDois(View view) {
        if (operador.isEmpty()) {
            part1 = part1 + "2";
        } else {
            part2 = part2 + "2";
        }
        teste = true;
        equacao = equacao + "2";
        campoDeTextoResultado.setText(equacao);
    }

    public void botaoTres(View view) {
        if (operador.isEmpty()) {
            part1 = part1 + "3";
        } else {
            part2 = part2 + "3";
        }
        teste = true;
        equacao = equacao + "3";
        campoDeTextoResultado.setText(equacao);
    }

    public void botaoQuatro(View view) {
        if (operador.isEmpty()) {
            part1 = part1 + "4";
        } else {
            part2 = part2 + "4";
        }
        teste = true;
        equacao = equacao + "4";
        campoDeTextoResultado.setText(equacao);
    }

    public void botaoCinco(View view) {
        if (operador.isEmpty()) {
            part1 = part1 + "5";
        } else {
            part2 = part2 + "5";
        }
        teste = true;
        equacao = equacao + "5";
        campoDeTextoResultado.setText(equacao);
    }

    public void botaoSeis(View view) {
        if (operador.isEmpty()) {
            part1 = part1 + "6";
        } else {
            part2 = part2 + "6";
        }
        teste = true;
        equacao = equacao + "6";
        campoDeTextoResultado.setText(equacao);
    }

    public void botaoSete(View view) {
        if (operador.isEmpty()) {
            part1 = part1 + "7";
        } else {
            part2 = part2 + "7";
        }
        teste = true;
        equacao = equacao + "7";
        campoDeTextoResultado.setText(equacao);
    }

    public void botaoOito(View view) {
        if (operador.isEmpty()) {
            part1 = part1 + "8";
        } else {
            part2 = part2 + "8";
        }
        teste = true;
        equacao = equacao + "8";
        campoDeTextoResultado.setText(equacao);
    }

    public void botaoNove(View view) {
        if (operador.isEmpty()) {
            part1 = part1 + "9";
        } else {
            part2 = part2 + "9";
        }
        teste = true;
        equacao = equacao + "9";
        campoDeTextoResultado.setText(equacao);
    }

    public void botaoDecimal(View view) {
        if (operador.isEmpty()) {
            /* Se não existe ponto na string. */
            if (!(part1.contains("."))) {
                part1 = part1 + ".";
                equacao = equacao + ".";
                campoDeTextoResultado.setText(equacao);
            } else {
                Toast.makeText(MainActivity.this, "Ponto já existente!", Toast.LENGTH_SHORT).show(); /* Mensagem solicitando apagar nome. */
                Vibrator vibra = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibra.vibrate(500);
            }

        } else {
            if (!(part2.contains("."))) {
                part2 = part2 + ".";
                equacao = equacao + ".";
                campoDeTextoResultado.setText(equacao);
            } else {
                Toast.makeText(MainActivity.this, "Ponto já existente!", Toast.LENGTH_SHORT).show(); /* Mensagem solicitando apagar nome. */
                Vibrator vibra = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibra.vibrate(500);
            }
        }
    }

    public void botaoSoma(View view) {
        /* Se a equação não tem operador de soma. */
        if (!(equacao.contains("+")) && (status == false)) {

            if ((equacao.contains("")) && (teste == false)) {
                Toast.makeText(MainActivity.this, "Insira um número!", Toast.LENGTH_SHORT).show(); /* Mensagem solicitando apagar nome. */
                Vibrator vibra = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibra.vibrate(500);

            } else {
                equacao = equacao + "+";
                campoDeTextoResultado.setText(equacao);
                operador = "+";
                status = true;
            }

        } else {
            if ((equacao.contains(operador)) && (status == true)) {
                Toast.makeText(MainActivity.this, "Operador já existente!", Toast.LENGTH_SHORT).show(); /* Mensagem solicitando apagar nome. */
                Vibrator vibra = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibra.vibrate(500);
            }
        }
    }

    public void botaoSubtrai(View view) {
        if (!(equacao.contains("-")) && (status == false)) {
            if ((equacao.contains("")) && (teste == false)) {
                Toast.makeText(MainActivity.this, "Insira um número!", Toast.LENGTH_SHORT).show(); /* Mensagem solicitando apagar nome. */
                Vibrator vibra = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibra.vibrate(500);
            } else {
                equacao = equacao + "-";
                campoDeTextoResultado.setText(equacao);
                operador = "-";
                status = true;
            }

        } else {
            if ((equacao.contains(operador)) && (status == true)) {
                Toast.makeText(MainActivity.this, "Operador já existente!", Toast.LENGTH_SHORT).show(); /* Mensagem solicitando apagar nome. */
                Vibrator vibra = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibra.vibrate(500);
            }
        }
    }

    public void botaoMultiplica(View view) {
        if (!(equacao.contains("x")) && (status == false)) {
            if ((equacao.contains("")) && (teste == false)) {
                Toast.makeText(MainActivity.this, "Insira um número!", Toast.LENGTH_SHORT).show(); /* Mensagem solicitando apagar nome. */
                Vibrator vibra = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibra.vibrate(500);
            } else {
                equacao = equacao + "x";
                campoDeTextoResultado.setText(equacao);
                operador = "x";
                status = true;
            }
        } else {
            if ((equacao.contains(operador)) && (status == true)) {
                Toast.makeText(MainActivity.this, "Operador já existente!", Toast.LENGTH_SHORT).show(); /* Mensagem solicitando apagar nome. */
                Vibrator vibra = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibra.vibrate(500);
            }
        }
    }

    public void botaoDivide(View view) {
        if (!(equacao.contains("/")) && (status == false)) {
            if ((equacao.contains("")) && (teste == false)) {
                Toast.makeText(MainActivity.this, "Insira um número!", Toast.LENGTH_SHORT).show(); /* Mensagem solicitando apagar nome. */
                Vibrator vibra = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibra.vibrate(500);
            } else {
                equacao = equacao + "/";
                campoDeTextoResultado.setText(equacao);
                operador = "/";
                status = true;
            }


        } else {
            if ((equacao.contains(operador)) && (status == true)) {
                Toast.makeText(MainActivity.this, "Operador já existente!", Toast.LENGTH_SHORT).show(); /* Mensagem solicitando apagar nome. */
                Vibrator vibra = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibra.vibrate(500);
            }
        }
    }

    public void botaoPorcentagem(View view) {
        if (!(equacao.contains("%")) && (status == false)) {
            if ((equacao.contains("")) && (teste == false)) {
                Toast.makeText(MainActivity.this, "Insira um número!", Toast.LENGTH_SHORT).show(); /* Mensagem solicitando apagar nome. */
                Vibrator vibra = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibra.vibrate(500);
            } else {
                equacao = equacao + "%";
                campoDeTextoResultado.setText(equacao);
                operador = "%";
                valor1 = Double.parseDouble(part1);
                resultado = valor1 / 100;
                equacao = Double.toString(resultado);
                part1 = Double.toString(resultado);
                part2 = "";
                campoDeTextoResultado.setText("" + resultado);
            }

        } else {
            if ((equacao.contains(operador)) && (status == true)) {
                Toast.makeText(MainActivity.this, "Operador já existente!", Toast.LENGTH_SHORT).show(); /* Mensagem solicitando apagar nome. */
                Vibrator vibra = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibra.vibrate(500);
            }
        }
    }


    public void botaoLimpa(View view) {
        equacao = "";
        campoDeTextoResultado.setText(equacao);
        resultado = 0;
        part1 = "0";
        part2 = "0";
        operador = "";
        status = false;
        teste = false;
    }

    public void botaoDesfazer(View view) {
        String equacao_sub = equacao.substring(equacao.length() - 1, equacao.length());
        /* Se equação tiver mais que um numero*/
        if (equacao.length() > 1) {
            /* Se a substring for algum operador. */
            if (equacao_sub == "+" || equacao_sub == "-" || equacao_sub == "x" || equacao_sub == "/") {
                equacao_sub = equacao.substring(0, equacao.length() - 1); // Reduz em -1.
                operador = ""; // Limpa operador.
                equacao = equacao_sub; // Substring passa a ser igual a equação.
            } else {
                /* Se tiver segundo número inserido.*/
                if (!(part2.isEmpty())) {
                    equacao_sub = equacao.substring(0, equacao.length() - 1);
                    part2 = part2.substring(0, part2.length() - 1);
                    equacao = equacao_sub;
                    /* Se não tiver segundo número inserido, vi diminuir somente do primeiro número.*/
                } else {
                    equacao_sub = equacao.substring(0, equacao.length() - 1);
                    part1 = part1.substring(0, part1.length() - 1);
                    equacao = equacao_sub;
                }
            }
        }
        campoDeTextoResultado.setText(equacao);
    }

    public void botaoResultado(View view) {
        if (equacao.isEmpty()) {
            part1 = "0";
            part2 = "0";
        }
        status = false;
        valor1 = Double.parseDouble(part1);
        if (!(part2.isEmpty())) {
            valor2 = Double.parseDouble(part2);
        }
        if (operador == "+") {
            resultado = valor1 + valor2;
        }
        if (operador == "-") {
            resultado = valor1 - valor2;
        }
        if (operador == "x") {
            resultado = valor1 * valor2;
        }
        if (operador == "/") {
            resultado = valor1 / valor2;
        }

        equacao = Double.toString(resultado);
        part1 = Double.toString(resultado);
        part2 = "";
        campoDeTextoResultado.setText("" + resultado);
    }

}

